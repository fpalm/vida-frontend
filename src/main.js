// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import vuetify from '@/plugins/vuetify'
import { i18n } from '@/plugins/i18n'
import FlagIcon from 'vue-flag-icon'
import VeeValidate from 'vee-validate'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(FlagIcon)
Vue.use(VeeValidate)

VeeValidate.Validator.extend('verify_password', {
  getMessage: field => 'The password must contain at least: 1 uppercase letter, 1 lowercase letter, 1 number, and one special character (E.g. , . _ & ? etc)',
  validate: value => {
    var strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})')
    return strongRegex.test(value)
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  vuetify,
  i18n,
  components: { App },
  template: '<App/>'
})
