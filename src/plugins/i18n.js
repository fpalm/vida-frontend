import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'en', // set locale
  fallbackLocale: 'es', // set fallback locale
  messages: {
    'en': {
      msg1: 'Welcome to Your Vue.js App',
      cancel: 'Cancel',
      email: 'E-mail',
      login: 'Login',
      confirmpassword: 'Confirm Password',
      username: 'Username',
      password: 'Password',
      register: 'Register',
      save: 'Save'
    },
    'es': {
      msg1: 'Bienvenido a tu aplicación Vue.js',
      cancel: 'Cancelar',
      email: 'Correo Electrónico',
      login: 'Entrar',
      confirmpassword: 'Confirme Contraseña',
      username: 'Usuario',
      password: 'Contraseña',
      register: 'Registro',
      save: 'Guardar'
    }
  } // set locale messages
})
