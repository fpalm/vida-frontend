import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// import connection from './modules/connection'
// import login from './modules/login'

Vue.use(Vuex)

// Make Axios play nice with Django CSRF
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

export default new Vuex.Store({

  state: {
    authUser: {},
    isAuthenticated: false,
    jwt: localStorage.getItem('token'),
    endpoints: {
      // TODO: Remove hardcoding of dev endpoints
      obtainJWT: 'http://127.0.0.1:8000/api/v1/token/',
      refreshJWT: 'http://127.0.0.1:8000/api/v1/token/refresh/',
      baseUrl: 'http://127.0.0.1:8000/api/v1/'
    },
    initials: '',
    // For connections
    connectionItems: []
  },

  mutations: {
    setAuthUser (state, {
      authUser,
      isAuthenticated
    }) {
      Vue.set(state, 'authUser', authUser)
      Vue.set(state, 'isAuthenticated', isAuthenticated)
      var ini = authUser.first_name.charAt(0) + authUser.last_name.charAt(0)
      Vue.set(state, 'initials', ini)
    },
    updateToken (state, newToken) {
      // TODO: For security purposes, take localStorage out of the project.
      localStorage.setItem('token', newToken)
      state.jwt = newToken
    },
    removeToken (state) {
      // TODO: For security purposes, take localStorage out of the project.
      localStorage.removeItem('token')
      state.jwt = null
    },
    // For connections
    UPDATE_CONNECTION_ITEMS (state, payload) {
      state.connectionItems = payload
    }
  },
  // For connections
  actions: {

    getConnectionItems ({ commit }) {
      axios.get('http://127.0.0.1:8000/api/v1/connections/', {
        headers: {Authorization: 'Bearer ' + this.state.jwt,
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        commit('UPDATE_CONNECTION_ITEMS', response.data)
      })
    }
  },
  // For connections
  getters: {
    connectionItems: state => state.connectionItems
  }

// TODO: separate Vuex store in modules
/*   state: {},
  getters: {},
  mutations: {},
  actions: {},

  modules: {
    connection,
    login
  } */

})
