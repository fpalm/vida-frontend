import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Helloworld from '@/components/HelloWorld'
import Register from '@/components/Register'
import Mainpage from '@/components/MainPage'
import Profile from '@/components/Profile'
import Encounters from '@/components/Encounters'
import Newencounter from '@/components/NewEncounter'
import Newconnection from '@/components/NewConnection'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Helloworld
    },
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/mainpage',
      name: 'Mainpage',
      component: Mainpage
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/encounters',
      name: 'Encounters',
      component: Encounters
    },
    {
      path: '/new_encounter',
      name: 'NewEncounter',
      component: Newencounter
    },
    {
      path: '/new_connection',
      name: 'NewConnection',
      component: Newconnection
    }
  ]
})
